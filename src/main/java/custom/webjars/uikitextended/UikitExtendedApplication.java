package custom.webjars.uikitextended;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UikitExtendedApplication {

    public static void main(String[] args) {
        SpringApplication.run(UikitExtendedApplication.class, args);
    }

}

package custom.webjars.uikitextended.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Country {
    private String name;
    private String code;
}

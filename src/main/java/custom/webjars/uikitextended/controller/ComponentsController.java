package custom.webjars.uikitextended.controller;

import custom.webjars.uikitextended.model.Country;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class ComponentsController {

    @GetMapping
    public String viewComponents() {
        return "components";
    }

    @GetMapping("/countries")
    @ResponseBody
    public List<Country> getCountries(@RequestParam(required = false) String countryName) {
        List<Country> countries = Arrays.asList(
                Country.builder().code("BG").name("Bulgaria").build(),
                Country.builder().code("AU").name("Australia").build(),
                Country.builder().code("BE").name("Belgium").build(),
                Country.builder().code("NG").name("Nigeria").build(),
                Country.builder().code("OM").name("Oman").build(),
                Country.builder().code("TR").name("Turkey").build(),
                Country.builder().code("UA").name("Ukraine").build(),
                Country.builder().code("RO").name("Romania").build()
        );
        return countries.stream().filter(country -> country.getName().contains(countryName)).collect(Collectors.toList());
    }

}

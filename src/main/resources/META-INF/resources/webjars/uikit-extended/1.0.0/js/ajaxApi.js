const AjaxCaller = {
    call: function (ajaxRequest, ajaxResponseAction) {
        if (ajaxResponseAction == null) {
            ajaxResponseAction = new AjaxResponseAction();
        }

        if (ajaxRequest.blockUI) {
            UIBlocker.show();
        }

        $.ajax({
            url: ajaxRequest.url,
            type: ajaxRequest.method,
            async: ajaxRequest.async,
            datatype: ajaxRequest.datatype,
            processData: ajaxRequest.processData,
            contentType: ajaxRequest.contentType,
            data: ajaxRequest.requestData,
        }).done(function (data) {
            if (ajaxResponseAction.validationFailedExpression != null && eval(ajaxResponseAction.validationFailedExpression)) {
                if (ajaxResponseAction.updateContainerOnInvalid != null && ajaxRequest.datatype === "html") {
                    $(ajaxResponseAction.updateContainerOnInvalid).html(data);
                    // initBindFirstValidation(ajaxResponseAction.updateContainerOnInvalid);
                }

                if (ajaxResponseAction.executeOnInvalid != null) {
                    const execOnInvalid = ExecutableFunctionBuilder.buildWithData(ajaxResponseAction.executeOnInvalid, data);
                    execOnInvalid();
                }
            } else {
                if (ajaxResponseAction.updateContainerOnValid != null && ajaxRequest.datatype === "html") {
                    $(ajaxResponseAction.updateContainerOnValid).html(data);
                    // initBindFirstValidation(ajaxResponseAction.updateContainerOnValid);
                }

                if (ajaxResponseAction.executeOnValid != null) {
                    const execOnValid = ExecutableFunctionBuilder.buildWithData(ajaxResponseAction.executeOnValid, data);
                    execOnValid();
                }
            }

            if (ajaxResponseAction.postprocessExecute != null) {
                const execPostprocess = ExecutableFunctionBuilder.buildWithData(ajaxResponseAction.postprocessExecute, data);
                execPostprocess();
            }
        }).fail(function (xhr, ajaxOptions, thrownError) {
            if (ajaxResponseAction.executeOnError != null) {
                const execError = ExecutableFunctionBuilder.buildWithData(ajaxResponseAction.executeOnError, xhr);
                execError();
            }
        }).always(function () {
            if (ajaxRequest.blockUI) {
                UIBlocker.hide();
            }
        });
    }
};

const AjaxRequest = function (url, options) {
    if (options === undefined || options === null) {
        options = {};
    }

    if (options.method == null) {
        options.method = "POST";
    }
    if (options.requestData == null) {
        options.requestData = {};
    }
    if (options.datatype == null) {
        options.datatype = "html";
    }
    if (options.blockUI == null) {
        options.blockUI = false;
    }
    if (options.processData == null) {
        options.processData = true;
    }
    if (options.contentType == null) {
        options.contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    }
    if (options.async == null) {
        options.async = true;
    }
    this.url = url;
    this.method = options.method;
    this.requestData = options.requestData;
    this.datatype = options.datatype;
    this.blockUI = options.blockUI;
    this.processData = options.processData;
    this.contentType = options.contentType;
    this.async = options.async;
};

const AjaxResponseAction = function (options) {
    this.validationFailedExpression = options.validationFailedExpression;
    this.updateContainerOnValid = options.updateContainerOnValid;
    this.updateContainerOnInvalid = options.updateContainerOnInvalid;
    this.executeOnValid = options.executeOnValid;
    this.executeOnInvalid = options.executeOnInvalid;
    this.postprocessExecute = options.postprocessExecute;
    this.executeOnError = options.executeOnError;
};


const FuncCall = function (func, args) {
    this.func = func;
    if (args === undefined || args === null) {
        this.args = [];
    } else {
        this.args = args;
    }
};

const ExecutableFunctionBuilder = {
    build: function (funcCall) {
        let args = Array.prototype.slice.call(funcCall.args, 0);
        return function () {
            return funcCall.func.apply(this, args);
        };
    },
    buildWithData: function (funcCall, data) {
        funcCall.args.push(data);
        return ExecutableFunctionBuilder.build(funcCall);
    },
};

const UIBlocker = {
    show: function () {
        $('.preloader').show();
    },
    hide: function () {
        $('.preloader').hide();
    }
}


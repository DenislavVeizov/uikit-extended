$(document).ready(function () {
    DatePicker.setLanguage();
    DatePicker.initAll();

    Select2.setLanguage()
    Select2.initAll()
});

var applicationContextPath = $('meta[name="_ctx"]').attr("content");//Important

var DatePicker = {
    initAll: function () {
        let $datepickers = $("[data-type='datepicker']");
        $datepickers.each(function (index, element) {
            DatePicker.init($(element));
        });
    },
    init: function ($element) {
        let label = $element.siblings('label');
        let maxDate = $element.attr('data-max-date');
        let minDate = $element.attr('data-min-date');
        let showButtons = $element.attr('data-show-buttons') === 'true';
        let showWeek = $element.attr('data-show-week') === 'true';
        let changeYear = $element.attr('data-change-year') === 'true';
        let changeMonth = $element.attr('data-change-month') === 'true';
        let showOtherMonths = $element.attr('data-show-other-months') === 'true';
        let selectOtherMonths = $element.attr('data-select-other-months') === 'true';
        $element.datepicker({
            showButtonPanel: showButtons,
            showWeek: showWeek,
            changeYear: changeYear,
            changeMonth: changeMonth,
            maxDate: maxDate,
            minDate: minDate,
            showOtherMonths: showOtherMonths,
            selectOtherMonths: selectOtherMonths,
            beforeShow: function (textbox, instance) {

            },
            onSelect: function () {
                if (label.length > 0) {
                    $(label[0]).addClass('active');
                }
            }
        });
        $element.focusin(function () {
            if (label.length > 0) {
                $(label[0]).addClass('active');
            }
        });
        $element.focusout(function () {
            if (label.length > 0 && $element.val() === '') {
                $(label[0]).removeClass('active');
            }
        });
    },
    setLanguage: function () {
        let currentLocale = $('#current-locale').attr('data-lang');
        if (currentLocale === 'ro') {
            $.datepicker.regional['ro'] = {
                closeText: "Închide",
                prevText: "&#xAB; Luna precedentă",
                nextText: "Luna următoare &#xBB;",
                currentText: "Azi",
                monthNames: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie",
                    "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
                monthNamesShort: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie",
                    "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
                dayNames: ["Duminică", "Luni", "Marţi", "Miercuri", "Joi", "Vineri", "Sâmbătă"],
                dayNamesShort: ["Dum", "Lun", "Mar", "Mie", "Joi", "Vin", "Sâm"],
                dayNamesMin: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ"],
                weekHeader: "Săpt",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ""
            };
            $.datepicker.setDefaults($.datepicker.regional['ro']);
        } else if (currentLocale === 'bg') {
            $.datepicker.regional['bg'] = {
                closeText: "затвори",
                prevText: "&#x3C;назад",
                nextText: "напред&#x3E;",
                nextBigText: "&#x3E;&#x3E;",
                currentText: "днес",
                monthNames: ["Януари", "Февруари", "Март", "Април", "Май", "Юни",
                    "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
                monthNamesShort: ["Януари", "Февруари", "Март", "Април", "Май", "Юни",
                    "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
                dayNames: ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"],
                dayNamesShort: ["Нед", "Пон", "Вто", "Сря", "Чет", "Пет", "Съб"],
                dayNamesMin: ["Не", "По", "Вт", "Ср", "Че", "Пе", "Съ"],
                weekHeader: "Wk",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ""
            };
            $.datepicker.setDefaults($.datepicker.regional['bg']);
        } else {
            $.datepicker.regional['en'] = {
                closeText: "Done",
                prevText: "Prev",
                nextText: "Next",
                currentText: "Today",
                monthNames: ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"],
                monthNamesShort: ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"],
                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                weekHeader: "Wk",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ""
            };

            $.datepicker.setDefaults($.datepicker.regional['en']);
        }
    }
}

var Select2 = {
    initAll: function () {
        let $elements = $("[data-type='select']");
        $elements.each(function (index, element) {
            Select2.init($(element));
        });
    },
    init: function ($element) {
        let currentLocale = $('#current-locale').attr('data-lang');
        if (currentLocale !== 'ro' && currentLocale !== 'bg') {
            currentLocale = 'en';
        }

        let minimumResultsForSearch = $element.attr('data-search-box') === 'true' ? null : -1;
        let placeholder = $element.attr('data-placeholder');
        $element.select2({
            language: currentLocale,
            minimumResultsForSearch: minimumResultsForSearch,
            placeholder: placeholder
        });
    },
    setLanguage: function () {
        let currentLocale = $('#current-locale').attr('data-lang');
        if (currentLocale === 'ro') {
            $.fn.select2.amd.define('select2/i18n/ro', [], function () {
                return {
                    errorLoading: function () {
                        return "Rezultatele nu au putut fi incărcate."
                    }, inputTooLong: function (e) {
                        var t = e.input.length - e.maximum, n = "Vă rugăm să ștergeți" + t + " caracter";
                        return 1 !== t && (n += "e"), n
                    }, inputTooShort: function (e) {
                        return "Vă rugăm să introduceți " + (e.minimum - e.input.length) + " sau mai multe caractere"
                    }, loadingMore: function () {
                        return "Se încarcă mai multe rezultate…"
                    }, maximumSelected: function (e) {
                        var t = "Aveți voie să selectați cel mult " + e.maximum;
                        return t += " element", 1 !== e.maximum && (t += "e"), t
                    }, noResults: function () {
                        return "Nu au fost găsite rezultate"
                    }, searching: function () {
                        return "Căutare…"
                    }, removeAllItems: function () {
                        return "Eliminați toate elementele"
                    }
                }
            });
        } else if (currentLocale === 'bg') {
            $.fn.select2.amd.define('select2/i18n/bg', [], function () {
                return {
                    inputTooLong: function (n) {
                        var e = n.input.length - n.maximum, u = "Моля въведете с " + e + " по-малко символ";
                        return e > 1 && (u += "a"), u
                    }, inputTooShort: function (n) {
                        var e = n.minimum - n.input.length, u = "Моля въведете още " + e + " символ";
                        return e > 1 && (u += "a"), u
                    }, loadingMore: function () {
                        return "Зареждат се още…"
                    }, maximumSelected: function (n) {
                        var e = "Можете да направите до " + n.maximum + " ";
                        return n.maximum > 1 ? e += "избора" : e += "избор", e
                    }, noResults: function () {
                        return "Няма намерени съвпадения"
                    }, searching: function () {
                        return "Търсене…"
                    }, removeAllItems: function () {
                        return "Премахнете всички елементи"
                    }
                }
            });
        } else {
            $.fn.select2.amd.define('select2/i18n/bg', [], function () {
                return {
                    errorLoading: function () {
                        return "The results could not be loaded."
                    }, inputTooLong: function (e) {
                        var n = e.input.length - e.maximum, r = "Please delete " + n + " character";
                        return 1 != n && (r += "s"), r
                    }, inputTooShort: function (e) {
                        return "Please enter " + (e.minimum - e.input.length) + " or more characters"
                    }, loadingMore: function () {
                        return "Loading more results…"
                    }, maximumSelected: function (e) {
                        var n = "You can only select " + e.maximum + " item";
                        return 1 != e.maximum && (n += "s"), n
                    }, noResults: function () {
                        return "No results found"
                    }, searching: function () {
                        return "Searching…"
                    }, removeAllItems: function () {
                        return "Remove all items"
                    }, removeItem: function () {
                        return "Remove item"
                    }, search: function () {
                        return "Search"
                    }
                }
            });
        }
    }
}
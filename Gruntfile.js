const sass = require('node-sass');

const sassConfig = {
    options: {
        implementation: sass,
        sourceMap: true
    },
    theme: {
        options: {outputStyle: 'compressed', sourceMap: false},
        files: {'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/css/uikit/uikit.css': 'uikit/theme/uikit.scss'}
    },
};
const copyConfig = {
    copyUikit: {
        files: [
            {
                expand: true,
                cwd: 'node_modules/uikit/dist/js/',
                src: ['uikit.js', 'uikit.min.js', 'uikit-icons.js', 'uikit-icons.min.js'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/js/uikit/'
            },
            {
                expand: true,
                cwd: 'node_modules/jquery/dist/',
                src: ['jquery.js', 'jquery.min.js'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/js/jquery/'
            },
            {
                expand: true,
                cwd: 'node_modules/jquery-ui-dist/',
                src: ['jquery-ui.js', 'jquery-ui.min.js'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/js/jquery-ui/'
            },
            {
                expand: true,
                cwd: 'node_modules/jquery-ui-dist/images/',
                src: ['**'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/css/jquery-ui/images'
            },
            {
                expand: true,
                cwd: 'node_modules/jquery-ui-dist/',
                src: ['jquery-ui.css','jquery-ui.min.css'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/css/jquery-ui/'
            },
            {
                expand: true,
                cwd: 'node_modules/select2/dist/css',
                src: ['select2.css','select2.min.css'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/css/select2/'
            },
            {
                expand: true,
                cwd: 'node_modules/select2/dist/js',
                src: ['select2.js','select2.min.js'],
                dest: 'src/main/resources/META-INF/resources/webjars/uikit-extended/1.0.0/js/select2/'
            },
        ],
    },
};
const notificationsConfig = {
    sass: {
        options: {
            enabled: true,
            message: 'Sass Compiled!',
            title: 'Uikit',
            success: true,
            duration: 1
        }
    },
    js: {
        options: {
            enabled: true,
            message: 'Javascript Compiled!',
            title: 'Uikit',
            success: true,
            duration: 1
        }
    },
};


module.exports = function (grunt) {
    grunt.initConfig({
        sass: sassConfig,
        copy: copyConfig,
        notify: notificationsConfig,
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('sass_compile',
        [
            'sass:theme',
            'notify:sass'
        ]
    );
    grunt.registerTask('js_compile',
        [
            'copy:copyUikit',
            'notify:js'
        ]
    );
    // grunt.registerTask('js_compile', ['uglify:main', 'notify:sass_compile']);
    grunt.registerTask('build', ['sass_compile', 'js_compile']);
};